// pages/map/map.js
var QQMapWX = require('../../../utils/qqmap-wx-jssdk');
var qqmapsdk;
Page({
  
  
  gotoindex:function(e){
    wx.switchTab({
      url: '../../index/index',
    })
},
  onLoad: function () {
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'NNNBZ-I76WF-G4JJY-NQEEO-SXLCQ-IZBDG'
    });
    this.mapCtx = wx.createMapContext('myMap')
  },
  //获取机器的信息data
  onShow: function(options){
    var that=this;
    wx.request({
      url: 'http://150.158.171.205:8080/vending/wxMachine/findAll',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res){
       that.setData({
         machine_data:res.data.extend.machine,
       })
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
      // 绑定事件使页面可以跳转到index/index页面
      gotoindex:function(e){
        wx.switchTab({
          url: '/index/index',
        })
    },

    // 附近机器(删除)
    dataList:[
      {
        machine_id:1,
        machine_name:'科学大道1号机 ',
        machine_juli:'150.12千米>',
        machine_icon:'https://s.pc.qq.com/tousu/img/20210424/6056255_1619233176.jpg',
        machine_text:'     150.12千米 >',
        machine_weizhi:'公交车站公交路旁50米',
      }
      // {
      //   machine_id:2,
      //   machine_name:'东风路科苑9号机',
      //   machine_icon:'https://s.pc.qq.com/tousu/img/20210424/6056255_1619233176.jpg',
      //   machine_weizhi:'东风路郑州轻工业家属院一号楼',
      // },
      // {
      //   machine_id:3,
      //   machine_name:'科学大道审美小区8号机',
      //   machine_icon:'https://s.pc.qq.com/tousu/img/20210424/6056255_1619233176.jpg',
      //   machine_weizhi:'公交车站东风路站旁50米',
      // },
      // {
      //   machine_id:4,
      //   machine_name:'东风路站台1号机',
      //   machine_icon:'https://s.pc.qq.com/tousu/img/20210424/6056255_1619233176.jpg',
      //   machine_weizhi:'公交车站公交路旁50米',
      // },
      // {
      //   machine_id:5,
      //   machine_name:'东风路站台1号机',
      //   machine_icon:'https://s.pc.qq.com/tousu/img/20210424/6056255_1619233176.jpg',
      //   machine_weizhi:'公交车站公交路旁50米',
      // }
    ],
    addmissage: '选的位置',
    // markers	 Array	标记点
    stitle:'故宫',
    latitude: "",
    longitude: "",
    scale: 14,
    markers: [],
    //controls控件 是左下角圆圈小图标,用户无论放大多少,点这里可以立刻回到当前定位(控件（更新一下,即将废弃，建议使用 cover-view 代替）)
    controls: [{
      id: 1,
      iconPath: '../../images/img/controls.png',
      position: {
        left: 15,
        top: 260 - 50,
        width: 40,
        height: 40
      },
      clickable: true
    }],
    distanceArr: []
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取当前的地理位置、速度
    wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        //赋值经纬度
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
 
        })
      }
    })
  },
  //controls控件的点击事件
  bindcontroltap(e) {
    var that = this;
    if (e.controlId == 1) {
      that.setData({
        latitude: this.data.latitude,
        longitude: this.data.longitude,
        scale: 14,
      })
    }
  },
})