// pages/medicine/medicine.js
var util=require('../../../utils/util.js')
 var app=getApp()
Page({
  data: {
    dname:null,
    mhid:null,
    salePrice:null,
    saleNumber:0,
    saleDate:null,
    saleUseropenid:null,
    drprice:0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //设置给页面显示的数据
      var that=this;
      that.setData({
        drname:options.drname,
        drnumber:options.drnumber,
        drspecifications:options.drspecifications,
        drfunction:options.drfunction,
        dimgurl:options.dimgurl,
        drprice:options.drprice,
        mhid:options.mhid
      });
  },
  pay:function(){
    var that=this;
    wx.showModal({
      cancelColor: '#949494',
      title: '提示',
      content: '是否要进行进行付款',
      success: function (res) {
          if(res.confirm) {
              //省略了付款操作，付款成功后保存购买信息,并让药品的数量-1
              let time=util.formatTime(new Date());
              that.data.saleNumber=1;
              that.setData({
                dname:that.data.drname,
                //日期完成，准备数据发送
                salePrice:that.data.drprice * that.data.saleNumber,
                saleDate:time,
                saleUseropenid:app.globalData.userOpenId
              })
              console.log(that.data.dname)
              console.log(that.data.mhid)
              console.log(that.data.salePrice);
              console.log(typeof(that.data.salePrice));
              console.log(that.data.saleNumber)
              console.log(that.data.saleDate)
              console.log(that.data.saleUseropenid)
              wx.request({
                url: 'http://localhost:8080/webMachine/wxSale/addSale',
                method: 'POST',
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"  
                },
                data:{
                  dname: that.data.dname,
                  mhid:that.data.mhid,
                  salePrice:that.data.salePrice,
                  saleNumber:that.data.saleNumber,
                  saleDate:that.data.saleDate,
                  saleUseropenid:that.data.saleUseropenid
                },
                success:function(res) {
                  if(res.data.code==100){
                    wx.navigateTo({
                      url: '/pages/NearbyMachines/medicinePayment/medicinePayment',
                    })
                  }
                }
              })      
          }
      }
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})