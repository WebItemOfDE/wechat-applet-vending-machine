// pages/medicine_list/medicine_list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    var mhid=parseInt(options.mhid);
    console.log(options.mhid);
    wx.request({
      url: 'http://150.158.171.205:8080/vending/wxDrug/findDrugByMachineId',
      header: {
        'content-type': 'application/json' // 默认值
      },
      data:{
        mhid:mhid
      },
      success: function(res){
        console.log(res.data.extend.drug);
       that.setData({
         drug_data:res.data.extend.drug,
         mhid:mhid
       })
      }
    })
  },
  onShow:function(options){
  
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})