// pages/web/index.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    devicePosition:'back',
    authCamera: false,//用户是否运行授权拍照
  },
  handleCameraError:function() {
    wx.showToast({
      title:'用户拒绝使用摄像头',
      icon: 'none'
    })
  },
  reverseCamera:function(){
    this.setData({
      devicePosition: "back" === this.data.devicePosition ? "front" : "back"
  });
  },
  takePhoto:function() {
    //拍摄照片
    wx.createCameraContext().takePhoto({
      quality: 'high',//拍摄质量(high:高质量 normal:普通质量 low:高质量)
      success: (res) => {
        //拍摄成功
        //照片文件的临时文件
        var file = res.tempImagePath;
        console.log(file)
        //上传图片到服务器
        wx.uploadFile({
          url: 'XXXX', //上传服务器地址
          filePath: file,
          name: 'file',
          formData: {
            'test': 'test'
          },
          success: (res) => {
            //上传成功
          },
          fail: function(t) {
            //上传失败
          },
        })
      },
      fail: (res) => {
        //拍摄失败
      },
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.getSetting({
      success: (res) => {
          if (res.authSetting["scope.camera"]) {
              this.setData({
                authCamera:true,
              })
          } else {
            this.setData({
              authCamera:false,
            })
            // wx.authorize({
            //   scope: 'scope.camera',
            //   success(){//同意授权
            //     this.setData({
            //       authCamera:true
            //     })
            //   },
            //   fail(){
            //     this.setData({
            //       authCamera:false
            //     })
            //     wx.showToast({
            //       title: '授权失败',
            //       icon:'none',
            //       duration:3000
            //     })
            //   }
            // })
            openCameraSetting();
          }
      }
    });
  },
  openCameraSetting(){
    wx.authorize({
      scope: 'scope.camera',
      success(){//同意授权
        this.setData({
          authCamera:true
        })
      },
      fail(){
        this.setData({
          authCamera:false
        })
        wx.showToast({
          title: '授权失败',
          icon:'none',
          duration:3000
        })
      }
    })
  }
})